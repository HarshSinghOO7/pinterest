from django.db import models
# Create your models here.


class Signup(models.Model):
    '''
    Table structure for newly added user
    '''

    user_id = models.AutoField(primary_key=True, null=False)
    user_name = models.CharField(max_length=200, null=False)
    user_email = models.CharField(max_length=200, null=False)
    user_password = models.CharField(max_length=200, null=False)
    user_age = models.IntegerField(null=False)

    def __str__(self):
        return str(self.user_name)


class Post(models.Model):
    '''
    Table structure for newly added post
    '''

    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(Signup, on_delete=models.CASCADE, null=False)
    user_name = models.CharField(max_length=200, null=False)
    user_post = models.FileField(upload_to='user_posts',
                                 null=False)  # for the image
    post_title = models.CharField(max_length=200)
    post_organise = models.CharField(max_length=200, null=True)
    post_topics = models.CharField(max_length=30, null=True)
    published_date = models.DateTimeField(auto_now_add=True, null=False)

    def __str__(self):
        return str(self.id)


class Save(models.Model):
    '''
    Table structure for saved post
    '''

    save_id = models.AutoField(primary_key=True)
    user = models.ForeignKey(Signup, on_delete=models.CASCADE, null=False)
    post = models.ForeignKey(Post, on_delete=models.CASCADE, null=False)

    def __str__(self):
        return str(self.post)


class Follow(models.Model):
    '''
    Table structure for follow user
    '''

    follow_id = models.AutoField(primary_key=True)
    # related_name is used to differentiate between both the users.
    follower = models.ForeignKey(
        Signup, on_delete=models.CASCADE, null=False, related_name='follower')
    following = models.ForeignKey(
        Signup, on_delete=models.CASCADE, null=False, related_name='following')

    def __str__(self):
        return str(self.follow_id)


class Comments(models.Model):
    '''
    Table structure for comments on post
    '''

    comment_id = models.AutoField(primary_key=True)
    post = models.ForeignKey(Post, on_delete=models.CASCADE, null=False)
    name = models.ForeignKey(Signup, on_delete=models.CASCADE, null=False)
    comment = models.TextField()
    date_added = models.DateTimeField(auto_now_add=True, null=False)

    def __str__(self):
        return str(self.comment_id)
