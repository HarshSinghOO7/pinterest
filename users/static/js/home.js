// to show drop down options
let dropDown = document.getElementById('drop-down');
dropDown.addEventListener('click', showOptions);

function showOptions() {
    document.getElementById('drop').style.display = 'inline-block';
}

let otherOptions = document.getElementById('other-options');
otherOptions.addEventListener('click', showOtherOptions);

function showOtherOptions() {
    document.getElementById('drop2').style.display = 'inline-block';
}

let home = document.getElementById("home");
let todayButton = document.getElementById("today");
let createButton = document.getElementById("create");
let icon1Button = document.getElementById("icon1");
let icon2Button = document.getElementById("icon2");
let icon3Button = document.getElementById("icon3");
let icon4Button = document.getElementById("icon4");


// function for home.
home.addEventListener('click', changeBackground1);

function changeBackground1() {
    home.style.color = "white";
    home.style.backgroundColor = "black";

    todayButton.style.backgroundColor = "white"
    todayButton.style.color = 'black';

    createButton.style.backgroundColor = "white";
    createButton.style.color = 'black';
}

// function for todayButton

todayButton.addEventListener('click', changeBackground2);

function changeBackground2() {
    todayButton.style.color = "white";
    todayButton.style.backgroundColor = "black";

    home.style.backgroundColor = "white"
    home.style.color = 'black';

    createButton.style.backgroundColor = "white";
    createButton.style.color = 'black';
}

//function for createButton

createButton.addEventListener('click', changeBackground3);

function changeBackground3() {
    createButton.style.color = "white";
    createButton.style.backgroundColor = "black";

    todayButton.style.backgroundColor = "white"
    todayButton.style.color = 'black';

    home.style.backgroundColor = "white";
    home.style.color = 'black';
}

// icon1Button.addEventListener('click', changeBackground);
// icon2Button.addEventListener('click', changeBackground);
// icon3Button.addEventListener('click', changeBackground);
// icon4Button.addEventListener('click', changeBackground);
// function changeBackground() {

//     createButton.style.color = "white";
//     createButton.style.backgroundColor = "black";

//     todayButton.style.color = 'white';
//     todayButton.style.backgroundColor = "black"

//     home.style.color = 'white';
//     home.style.backgroundColor = "black";

// }
