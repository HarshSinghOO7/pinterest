/*********************************************************** FORM SECTION ***********************************************************/

/************************** REGISTER SECTION ********************************/

// Taking Input from the register form
let registerUser = document.getElementById('continue-register');
registerUser.addEventListener('click', registerUserToDB);

function registerUserToDB() {
    let form = document.getElementById('form2');
    let email = document.getElementById('email-register').value;
    let password = document.getElementById('password-register').value;
    let age = document.getElementById('age-register').value;

    if (email.length > 12 && (password.length > 0 && password.length >= 8) && age >= 10) {
        form.submit();
    } else if (email.length < 10) {
        form.addEventListener('submit', function (event) {
            event.preventDefault(); // not letting the form getting auto-submit.
        })
        const emailError = document.getElementById('register-email-error');
        emailError.style.visibility = 'visible';
        setTimeout(() => {
            emailError.style.visibility = 'hidden';
        }, 4000);

    } else if (password.length < 8) {
        form.addEventListener('submit', function (event) {
            event.preventDefault(); // not letting the form getting auto-submit.
        })
        const passwordError = document.getElementById('register-password-error');
        passwordError.style.visibility = 'visible';
        setTimeout(() => {
            passwordError.style.visibility = 'hidden';
        }, 4000);
    } else if (age < 10) {
        form.addEventListener('submit', function (event) {
            event.preventDefault(); // not letting the form getting auto-submit.
        })
        const ageError = document.getElementById('register-age-error');
        ageError.style.visibility = 'visible';
        setTimeout(() => {
            ageError.style.visibility = 'hidden';
        }, 4000);
    }
}

// Taking Input from the Login form

let loginUser = document.getElementById('continue-login');
loginUser.addEventListener("click", loginUserToDB);

function loginUserToDB() {
    let loginForm = document.getElementById('form1');
    let email = document.getElementById('email-login');
    let password = document.getElementById('password-login');

    if (email.length > 12 && (password.length > 0 && password.length >= 8)) {
        loginForm.submit();
    } else if (email.length < 10) {
        loginForm.addEventListener('submit', function (event) {
            event.preventDefault(); // not letting the form getting auto-submit.
        });
        const emailError = document.getElementById('login-email-error');
        emailError.style.visibility = 'visible';
        setTimeout(() => {
            emailError.style.visibility = 'hidden';
        }, 4000);

    } else if (password.length < 8) {
        loginForm.addEventListener('submit', function (event) {
            event.preventDefault(); // not letting the form getting auto-submit.
        });
        const passwordError = document.getElementById('login-password-error');
        passwordError.style.visibility = 'visible';
        setTimeout(() => {
            passwordError.style.visibility = 'hidden';
        }, 4000);
    }
}

// When user clicks anywhere outside of the modal, close it

let registration = document.getElementById('form-reg');
let login = document.getElementById('log-form');

console.log(registration)
console.log(login)

window.addEventListener("click", function (event) {
    if (event.target === registration) {
        registration.style.display = 'none';
    }
    else if (event.target === login) {
        login.style.display = 'none';
    }
});
