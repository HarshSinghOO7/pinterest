let commentArea = document.getElementById('comments-area');
commentArea.addEventListener('focus', call);

function call() {
    console.log("I'm calling");
    document.getElementById('process').style.display = 'block';
}


// Comment buttons
let rightButton = document.getElementById('right');
let downButton = document.getElementById('down');

downButton.addEventListener('click', downButtonAction);

function downButtonAction() {
    document.getElementById('section-comments').style.display = 'none';
    rightButton.style.display = 'inline';
    downButton.style.display = 'none';
}

rightButton.addEventListener('click', rightButtonAction);

function rightButtonAction() {
    document.getElementById('section-comments').style.display = 'block';
    rightButton.style.display = 'none';
    downButton.style.display = 'inline';
}