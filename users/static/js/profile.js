let saved = document.getElementById('user-saved');
let created = document.getElementById('user-created');

saved.addEventListener('click', displaySaved);
created.addEventListener('click', displayCreated);

function displaySaved() {
    document.getElementById('user-created-pins').style.display = 'none';
    document.getElementById('user-saved-pins').style.display = 'block';
    saved.style.borderBottom = '2px solid black';
    created.style.borderBottom = "inherit";

}

function displayCreated() {
    document.getElementById('user-saved-pins').style.display = 'none';
    document.getElementById('user-created-pins').style.display = 'block';
    created.style.borderBottom = '2px solid black';
    saved.style.borderBottom = "inherit";
}
