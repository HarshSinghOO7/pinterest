from django.urls import path
from . import views

app_name = 'users'

urlpatterns = [
    path('', views.index_view, name='index'),
    path('register', views.register_view, name='Signup'),
    path('login', views.login_view, name='Login'),
    path('username', views.username_view, name='username'),
    path('home/<user>', views.home_view, name='home'),
    path('profile/<user>', views.profile_view, name='profile'),
    path('createpin/<user>', views.createpin_view, name='createpin'),
    path('post/<postid>', views.postdetails_view, name='postdetails'),
    path('follow/<user>', views.follow_view, name='follow'),
]
