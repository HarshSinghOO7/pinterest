from django.shortcuts import render, reverse
from .models import Signup, Post, Save, Follow, Comments
from django.http import HttpResponseRedirect
from datetime import date
from django.contrib.auth.models import User
from django.contrib.auth import logout, authenticate
from django.contrib import auth
# Create your views here.


######################  INDEX ##########################
def index_view(request):
    return render(request, 'index.html')


######################  REGISTER  ##########################
def register_view(request):
    '''
    Registering user when getting a post request and
    adding the user in the session and navigating to username.html file for further user details.

    Else if no post request has been made then return the register.html file.
    '''

    if request.method == 'POST':
        email = request.POST.get('email-re')
        password = request.POST.get('pass-re')
        age = int(request.POST.get('age'))

        # creating session of the email so that other pages can know which user it is.
        user_id = None
        user_id = Signup.objects.filter(user_email=email).values('user_id')

        if len(user_id) != 0:
            context = {
                'error': "User already exists !!!"
            }
            return render(request, 'register.html', context)

        else:
            request.session['user_email'] = email
            request.session['user_password'] = password
            request.session['user_age'] = age
            # to tell django that something has updated.
            # redirecting it to login form.
            return HttpResponseRedirect(reverse('users:username'))

    return render(request, 'register.html')


######################  USERNAME  ##########################
def username_view(request):
    '''
    Taking all the details passed throught the session and sending it to database.

    Else returning the username html file on request
    '''

    user_email = request.session.get('user_email')
    user_password = request.session.get('user_password')
    user_age = request.session.get('user_age')

    if request.method == 'POST':

        user_name = request.POST.get('username')
        request.session['user_name'] = user_name

        user = User.objects.create_user(
            username=user_name, password=user_password, email=user_email, first_name=user_name)
        user.save()

        new_user = Signup.objects.create(user_name=user_name, user_email=user_email,
                                         user_password=user_password, user_age=user_age)
        new_user.save()

        # getting the user_id and saving in session

        user_id = User.objects.get(email=user_email)
        request.session['user_id'] = user_id.id

        # deleting the not required sessions.
        try:
            request.session.pop('user_age')
            request.session.pop('user_password')
        except KeyError:
            pass

        return HttpResponseRedirect(reverse('users:home', kwargs={'user': user_name}))

    context = {
        'user_email': user_email,
        'user_password': user_password
    }
    return render(request, 'username.html', context)


######################  LOGIN  ##########################
def login_view(request):
    '''
    logging in the user by checking the db when post request is made.

    If no request is been made than returning the login page.
    '''

    if request.method == 'POST':

        email = request.POST.get('email')
        password = request.POST.get('pass')

        try:
            username = Signup.objects.get(user_email=email)
        except Signup.DoesNotExist:

            context = {
                'error': "Invalid Credentials !!!"
            }
            return render(request, 'login.html', context)

        user_name = username.user_name
        user = auth.authenticate(
            request, username=user_name, password=password)
        auth.login(request, user)
        user_id = Signup.objects.filter(user_email=email).values('user_id')
        user_id = user_id[0]['user_id']

        # checking if the users exists.

        user_name = None
        user_name = Signup.objects.filter(
            user_email=email).values('user_name')
        user_name = user_name[0]['user_name']

        request.session['user_id'] = user_id
        request.session['user_email'] = email
        request.session['user_name'] = user_name

        return HttpResponseRedirect(reverse('users:home', kwargs={'user': user_name}))

    return render(request, 'login.html')

######################  HOME  ##########################


def home_view(request, user):
    '''
    Return the username html file on request
    '''

    user_name = request.session.get('user_name')
    user_id = request.session.get('user_id')

    if request.method == 'POST':

        if 'save-post' in request.POST:

            # taking post and user id
            post_id = request.POST.get('post-id')
            user_id = request.session.get('user_id')

            # checking if it is present already in the database
            save_is_present = Save.objects.filter(
                user=user_id).filter(post=post_id)
            users_have_posted = Post.objects.filter(
                user=user_id).filter(id=post_id)

            # to save only that post which is not created by the same user.
            if len(save_is_present) == 0 and len(users_have_posted) == 0:
                id = Signup.objects.get(user_id=user_id)
                post = Post.objects.get(id=post_id)
                new_save = Save(user=id, post=post)
                new_save.save()

        else:
            logout(request)
            return HttpResponseRedirect(reverse('users:index'))

    try:
        request.session.pop('postid')
    except KeyError:
        pass

    posts = Post.objects.all().order_by('-published_date')
    user = user_name
    context = {
        'user_name': user_name,
        'posts': posts,
        'user_id': user_id,
    }
    return render(request, 'home.html', context)


######################  PROFILE  ##########################
def profile_view(request, user):
    '''
    Return the profile.html file on request and sending the context.
    '''

    if user != str(request.user) and 'postid' in request.session:

        user_post = Post.objects.all().filter(id=request.session['postid'])
        user_follow = None
        for other_user in user_post:
            user_follow = other_user.user_id

        other_user_details = Signup.objects.get(user_id=user_follow)
        id = other_user_details.user_id
        name = other_user_details.user_name
        email = other_user_details.user_email
        post = Post.objects.all().filter(user=other_user_details.user_id)

        user_id = Signup.objects.get(user_id=request.user.id)

        check_following = Follow.objects.filter(
            follower=user_id, following=id)

        check_following = len(check_following)

        follow_count = Follow.objects.filter(following=id).count()
        following_count = Follow.objects.filter(follower=id).count()

        saved_post = Save.objects.filter(user=id)

        context = {
            'id': id,
            'already_followed': check_following,
            'posts': post,
            'user_email': email,
            'user_name': name,
            'follow_count': follow_count,
            'following_count': following_count,
            'savedpost': saved_post
        }
        user = name
        return render(request, 'profile.html', context)

    else:
        try:
            request.session.pop('postid')
        except KeyError:
            pass

        posts = Post.objects.all().filter(user_id=request.session.get('user_id'))
        user_email = request.session['user_email']
        user_name = request.session['user_name']
        follow_count = Follow.objects.filter(following=request.user.id).count()
        following_count = Follow.objects.filter(
            follower=request.user.id).count()

        saved_post = Save.objects.filter(user=request.user.id)

        context = {
            'user_email': user_email,
            'user_name': user_name,
            'posts': posts,
            'follow_count': follow_count,
            'following_count': following_count,
            'savedpost': saved_post
        }
        return render(request, 'profile.html', context)


######################  CREATEPIN  ##########################
def createpin_view(request, user):
    '''
    Made for creating the pins(posting images.)
    '''

    try:
        request.session.pop('postid')
    except KeyError:
        pass

    if request.method == 'POST':
        id = Signup.objects.get(user_id=request.session.get('user_id'))
        post = request.FILES.get('image-file')
        title = request.POST.get('title')
        organise = request.POST.get('organise')
        topics = request.POST.get('topics')
        name = request.session.get('user_name')

        # checking if the organise field is empty or not and saving it accordingly.
        if organise != None:

            if topics != None:
                new_post = Post(user=id, user_name=name, user_post=post, post_title=title,
                                post_organise=organise, post_topics=topics)
                new_post.save()
                return HttpResponseRedirect(reverse('users:profile', kwargs={'user': name}))

            else:
                new_post = Post(user=id, user_name=name, user_post=post, post_title=title,
                                post_organise=organise)
                new_post.save()
                return HttpResponseRedirect(reverse('users:profile', kwargs={'user': name}))

        else:
            new_post = Post(user=id, user_name=name,
                            user_post=post, post_title=title)
            new_post.save()
            return HttpResponseRedirect(reverse('users:profile', kwargs={'user': name}))

    user_id = request.session['user_id']
    user_name = request.session['user_name']
    user = user_name

    context = {
        'user_id': user_id,
        'user_name': user_name,
    }
    return render(request, 'createpin.html', context)

######################  POST DETAILS  ##########################


def postdetails_view(request, postid):
    '''
    Showing the complete detail of the post as well as handling comments.
    '''

    try:
        request.session.pop('postid')
    except KeyError:
        pass

    if request.method == 'POST':

        if 'save' in request.POST:
            # taking post and user id
            user_id = request.session.get('user_id')

            # checking if it is present already in the database
            save_is_present = Save.objects.filter(
                user=user_id).filter(post=postid)
            users_have_posted = Post.objects.filter(
                user=user_id).filter(id=postid)

            # to save only that post which is not created by the same user.
            if len(save_is_present) == 0 and len(users_have_posted) == 0:
                id = Signup.objects.get(user_id=user_id)
                post = Post.objects.get(id=postid)
                new_save = Save(user=id, post=post)
                new_save.save()
                print("Saved")

        elif 'done' in request.POST or 'save' not in request.POST:
            comment = request.POST.get('comment')
            if len(comment) != 0:
                user_name = Signup.objects.filter(
                    user_name=request.session['user_name'])

                post = Post.objects.filter(id=postid)

                new_comment = Comments.objects.create(
                    post=post[0], name=user_name[0], comment=comment)

                new_comment.save()

    user_id = request.session['user_id']
    user_name = request.session['user_name']
    request.session['postid'] = postid

    post = Post.objects.all().filter(id=postid)
    allposts = Post.objects.all().order_by('-published_date')
    comments = Comments.objects.all().filter(post=postid)

    # getting the name of the other user
    other_user = Post.objects.filter(id=postid).values('user')
    other_user_id = other_user[0]['user']

    count = Follow.objects.filter(following=other_user_id).count()
    context = {
        'user_id': user_id,
        'user_name': user_name,
        'post': post,
        'allposts': allposts,
        'comments': comments,
        'count': len(comments),
        'follow': count,
    }
    return render(request, 'postdetails.html', context)


######################  FOLLOW USER  ##########################

def follow_view(request, user):
    username = Signup.objects.get(user_name=user)
    loggedin_user = Signup.objects.get(user_name=request.session['user_name'])

    f, create = Follow.objects.get_or_create(
        follower=loggedin_user, following=username)

    if create == False:
        f.delete()

    return HttpResponseRedirect(reverse('users:profile', kwargs={'user': user}))
